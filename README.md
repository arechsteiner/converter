# Rails + Vue Currency Converter

## Demo

A demo can be found at http://converter.wildmind.ch

## Special Prerequisites

Converter uses the Open Exchange Rates API, so in order to be functional an API key is needed. A free account
which provides hourly updated rates based on USD is sufficient. Other pair rates are calculated based on the
USD rates provided by the API.

## Installation

1. install gems: `bundle install`
1. install JS packages: `yarn install`
1. configure the api key environment variable (see below)
1. `rails server`

### Api Key

The API key must be available as an environment variable `OPEN_EXCHANGE_RATES_KEY=insert_key_here`. For development
and test this can be achieved using the dotenv gem. Simply rename the file `.env.local.example` to `.env.local` and 
fill in your api key.

## Tests

There are two test suites: Rspec and Mocha. 

## Rails / rspec

`bundle exec rspec` runs the Rails request and system tests. API calls are mocked by VCR/webmock so no calls to the 
actual API will be made.


## Vue / mocha

`yarn test` runs the mocha tests which unit tests the Vue components. API calls to the Rails application are mocked by
Sinon so no calls to the rails application or the remote API will be made.
