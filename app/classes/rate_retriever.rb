require 'money/bank/open_exchange_rates_bank'
require 'date'
require 'fileutils'

class RateRetriever
  def get_rate(from_cur, to_cur, date = nil)
    #return rand(0.81..1.41) # for dev purposes to not exhaust api

    oxr = Money::Bank::OpenExchangeRatesBank.new
    oxr.app_id = ENV['OPEN_EXCHANGE_RATES_KEY']

    if date.present?
      if Date.parse(date) > Date.parse("1999-01-01")
        oxr.date = date
        oxr.cache = get_cache_for_date(date)
      else
        # TODO: error handling
      end
    else
      # TODO: figure out reasonable caching for today-values (e.g. hourly)
    end

    oxr.update_rates
    oxr.get_rate(from_cur, to_cur)
  end

  # returns a cache dir for a date
  # creates directory if needed
  def get_cache_for_date(date)
    parsed_date = Date.parse(date)
    cache_dir = 'cache/' + parsed_date.year.to_s + '/' + parsed_date.month.to_s + '/'
    cache_file = 'oxr-cache-' + date.to_s + '.json'

    FileUtils.mkdir_p cache_dir

    cache_dir + cache_file
  end
end