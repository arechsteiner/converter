require 'date'

class Api::V1::RatesController < ApplicationController
  def rate
    rr = RateRetriever.new

    if params[:from].present? && params[:to].present?

      if params[:date].present?
        rate =  rr.get_rate(params[:from], params[:to], params[:date])
      else
        rate =  rr.get_rate(params[:from], params[:to])
      end

      d = DateTime.now

      render :json => { "rate" => rate, "timestamp" => d.strftime("%d/%m/%Y %H:%M") }, status: :ok

    else
      render :json => { "error" => "You must provide from and to currencies as parameters"}, status: :not_acceptable
    end

  end
end
