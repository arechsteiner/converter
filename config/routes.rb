Rails.application.routes.draw do
  get 'home/index'

  root 'home#index'

  namespace :api do
    namespace :v1 do
      get 'rate' => 'rates#rate'
      get 'rate/:date/' => 'rates#rate'
    end
  end
end
