require 'rails_helper'

RSpec.describe HomeController, type: :controller do

  describe "GET index" do
    it "renders homepage with title" do
      get :index
      expect(response).to have_http_status(:ok)
    end
  end

end
