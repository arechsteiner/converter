require 'rails_helper'

RSpec.describe "Api::V1::Rates", type: :request do
  describe "GET /api/v1/rate" do
    it "throws and error if no currencies are supplied" do
      get "/api/v1/rate"
      expect(response).to have_http_status(:not_acceptable)
    end

    it "returns rate for USD and CHF" do
      VCR.use_cassette("oxr_latest", :match_requests_on => [:host, :path]) do
        get "/api/v1/rate?from=USD&to=CHF"
        body = JSON.parse(response.body)

        expect(response).to have_http_status(200)
        expect(body["rate"].to_f).to be_within(0.01).of(0.969122)
        expect(Time.parse(body["timestamp"])).to be_within(2.minutes).of(Time.now)
      end
    end

    it "returns rate for MXN and EUR" do
      VCR.use_cassette("oxr_latest", :match_requests_on => [:host, :path]) do
        get "/api/v1/rate?from=MXN&to=EUR"

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["rate"].to_f).to be_within(0.01).of(0.0476205)
      end
    end
  end

  describe "GET /api/v1/rate/:date" do
    it "returns rate for USD and CHF for 2010-01-01" do
      VCR.use_cassette("oxr_2010_01_01", :match_requests_on => [:host, :path] ) do
        get "/api/v1/rate/2010-01-01?from=USD&to=CHF"
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["rate"].to_f).to be_within(0.01).of(1.0338)
      end
    end

    it "returns rate for EUR and CHF for 2010-01-01" do
      VCR.use_cassette("oxr_2010_01_01", :match_requests_on => [:host, :path]) do
        get "/api/v1/rate/2010-01-01?from=EUR&to=CHF"
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["rate"].to_f).to be_within(0.01).of(1.48267)
      end
    end

    it "caches a historical rate" do
      VCR.use_cassette("oxr_2010_01_01", :match_requests_on => [:host, :path], :allow_playback_repeats => true) do

        cache_path = "cache/2010/1/oxr-cache-2010-01-01.json"
        File.delete(cache_path) if File.exist?(cache_path)

        get "/api/v1/rate/2010-01-01?from=EUR&to=CHF"
        get "/api/v1/rate/2010-01-01?from=EUR&to=CHF"

        pattern = /https:\/\/openexchangerates.org\/api\/historical\/2010-01-01.json.*/
        expect(a_request(:get, pattern)).to have_been_made.once
      end
    end
  end
end
