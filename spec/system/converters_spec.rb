require 'rails_helper'

RSpec.describe "Converters", type: :system do
  before do
    driven_by(:selenium_chrome_headless)
  end

  it "enables me to convert a currency with today's rate" do
    VCR.use_cassette("oxr_latest", :match_requests_on => [:host, :path], :allow_playback_repeats => true) do
      visit "/"
      expect(page).to have_text("Currency Exchange")
      expect(page).to have_text("0.92133")

      fill_in "Convert amount", with: "99"

      result = page.find("#ce-conversion-result")

      expect(result).to have_text("91.21")

      click_link "Flip currencies"

      expect(result).to have_text("107.45")
    end
  end

  it "enables me to convert a currency with a historical rate" do
    VCR.use_cassette("oxr_latest", :match_requests_on => [:host, :path], :allow_playback_repeats => true) do
      visit "/"
      # TODO
    end
  end
end