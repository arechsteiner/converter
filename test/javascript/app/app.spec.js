import { mount } from 'vue-test-utils';
import chai from 'chai';
import App from 'app';
import flatPickr from "vue-flatpickr-component";
import sinon from 'sinon';
import axios from 'axios';
import Vue from 'vue';

const expect = chai.expect;

describe ('App', () => {
    let wrapper;
    let stub;

    beforeEach(() => {
        let rateResponse = {
            status: 200,
            data: {
                rate: 0.777,
                timestamp: '18/12/2019 11:31'
            }
        };

        let currenciesResponse = {
            status: 200,
            data: {
                "AED": "United Arab Emirates Dirham",
                "AFN": "Afghan Afghani",
                "ALL": "Albanian Lek",
                "AMD": "Armenian Dram",
                "ANG": "Netherlands Antillean Guilder",
                "AOA": "Angolan Kwanza",
                "ARS": "Argentine Peso"
            }
        };

        stub = sinon.stub(axios, 'get');

        stub.withArgs("https://openexchangerates.org/api/currencies.json")
            .resolves(Promise.resolve(currenciesResponse));

        stub.withArgs("/api/v1/rate?from=CHF&to=EUR")
            .resolves(Promise.resolve(rateResponse));

        wrapper = mount(App);
    });

    afterEach(() => {
        stub.restore();
        sinon.restore();
    });

    it('initializes with the correct rate', async () => {
        await Vue.nextTick();
        expect(wrapper.vm.rate).to.be.equal(0.777);
    });

    it('initializes the currencies', async () => {
        await Vue.nextTick();

        expect(wrapper.vm.currencies[0].value).to.be.equal("AED");
        expect(wrapper.vm.currencies[0].label).to.be.equal("United Arab Emirates Dirham (AED)");
    });

    it('outputs a correct, rounded amount', () => {
        wrapper.setData({ rate: 0.777777});
        enterAmount(1);
        expect (wrapper.find('#ce-amount-output .ce-formfield-field h1').text()).to.be.equal("0.78");
    });

    it('flips currencies and displays the correct rate', async () => {
        let response = {
            status: 200,
            data: {
                rate: 1.287,
                timestamp: '18/12/2019 11:31'
            }
        };

        stub.withArgs("/api/v1/rate?from=EUR&to=CHF")
            .resolves(Promise.resolve(response));

        wrapper.find('.ce-flipper a').trigger('click');

        await Vue.nextTick();

        expect(wrapper.vm.rate).to.be.equal(1.287);
    });

    it('retrieves a historical rate on date selection', async () => {
        let response = {
            status: 200,
            data: {
                rate: 1.187,
                timestamp: '18/12/2019 11:31'
            }
        };

        stub.withArgs("/api/v1/rate/2019-12-04?from=CHF&to=EUR")
            .resolves(Promise.resolve(response));

        //wrapper.find(flatPickr).vm.fp.setDate("2019-12-04", true); // causes trouble on VPS but not locally (?)

        // Alternative way of setting the date, bypassing flatpickr instance:
        wrapper.setData({
            params: {
                ...wrapper.vm.params,
                date: "2019-12-04"
            }
        });

        await Vue.nextTick();

        expect(wrapper.vm.params.date).to.be.equal("2019-12-04");
        expect(wrapper.vm.rate).to.be.equal(1.187);
    });

    function enterAmount(amount) {
       let amountInput = wrapper.find('#ce-amount-input input');
       amountInput.element.value = amount;
       amountInput.trigger('input');
    }
});
